" Vim color file
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last Change:	2001 Jul 23

" This is the default color scheme.  It doesn't define the Normal
" highlighting, it uses whatever the colors used to be.

" Set 'background' back to the default.  The value can't always be estimated
" and is then guessed.
hi clear Normal
set bg&

" Remove all existing highlighting and set the defaults.
hi clear

" Load the syntax highlighting defaults, if it's enabled.
if exists("syntax_on")
  syntax reset
endif

let colors_name = "theme"

" vim: sw=2
hi PreCondit ctermbg=NONE ctermfg=10 "Dentro de begin{} end{}
"hi SpecialChar "\\
hi LineNR ctermbg=NONE ctermfg=1 "numeros del lado
hi Statement ctermfg=2 cterm=bold "\begin \end \includegraphics...
" hi Number ctermfg=11 "scale=0.23
" hi Type ctermfg=1 "textbf, textit... fuera del texto y \subset...
hi MatchParen ctermbg=NONE ctermfg=214 cterm=underline,bold "highlight de la pareja
hi Folded ctermbg=202 ctermfg=15
"hi Storage ctermbg=NONE ctermfg=3

hi SpellBad  ctermbg=NONE ctermfg=160 cterm=underline
hi SpellRare  ctermbg=NONE ctermfg=125 cterm=underline
hi SpellCap  ctermbg=NONE ctermfg=125 cterm=underline
hi SpellLocal  ctermbg=NONE ctermfg=125 cterm=underline
hi Delimiter  ctermbg=NONE ctermfg=10
hi Comment  ctermbg=NONE ctermfg=186 
hi Visual  ctermbg=7 ctermfg=0 cterm=bold
hi Search  ctermbg=10 ctermfg=0
hi IncSearch  ctermbg=196 ctermfg=15 cterm=bold
hi Conceal ctermfg=2 ctermbg=NONE
hi Special ctermfg=2 "letras en ecuaciones
hi PreProc ctermfg=10 ctermbg=NONE " Path en includegraphics
" highlight Normal ctermfg=15
hi Constant ctermfg=10 ctermbg=NONE cterm=bold
hi identifier ctermfg=10 "Entre corchetes []
