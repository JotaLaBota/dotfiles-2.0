cp .lightdmconfig/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf
cp .lightdmconfig/Mint-Y-Dark-Red /usr/share/themes/Mint-Y-Dark-Red/
cp .lightdmconfig/kakashi.png /usr/share/backgrounds/kakashi.png
cp .lightdmconfig/accserv /var/lib/AccountsService/
systemctl enable lightdm.service
