# Paquetes a instalar:
## Window manager
Para poder tener un entorno gráfico. He elegido spectrwm como window manager
```
xorg xorg-server xorg-xinit spectrwm ttf-ubuntu-font-family noto-fonts #Algunas fuentes vienen bien
```
## Terminal
```
kitty 
```

## Editor de texto
Este paquete hace falta para poder usar el clipboard en vim
```
gvim 
```

## Gestor de archivos
Para previsualizar imágenes y vídeos en vifm
```
vifm 
ueberzug 
ffmpegthumbnailer
```
El gestor gráfico y lxappearance para los temas gtk
```
nemo
nemo-fileroller
lxappearance
```

## Audio
```
pulseaudio 
pavucontrol
pamixer
mpv
youtube-dl
```

## Lector de pdf
```
zathura
zathura-pdf-poppler
zathura-djvu
```


## Fotografía
```
flameshot
feh
```

## Display manager
```
lightdm
lightdm-gtk-greeter
```

## Navegador
```
qutebrowser
```

## Otros
```
git
wget
dmenu
picom
pass
discord
neofetch
gpick
redshift
fzf
xdg-user-dirs
python-pywal
texlive-most
```




## [AUR helper](https://github.com/morganamilo/paru)
```
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

## Programas del AUR
```
i3lock-fancy
jmtpfs
```


## Programas externos
```
pacman -U megasync
```


## [Fuentes externas](https://www.nerdfonts.com/font-downloads)
Hay que descargar UbuntuMono Nerd font
```
cp font /usr/share/fonts
```


# Clonar la configuración
```
git clone https://gitlab.com/JotaLaBota/dotfiles-2.0.git
```


1. Movemos los archivos que queramos a nuestro home
```
cd dotfiles-2.0
cp file ~/
```


2. Configuramos lightdm
```
cd .lightdmconfig
copiamos los archivos según aparece en el readme.txt
systemctl enable lightdm.service
```



3. Seleccionamos keyboard layout por defecto una vez entramos en spectrwm
```
localectl --no-convert set-x11-keymap es
```



4. Abrimos lxappearance para cambiar el tema
```
cd .themes
tar -xvzf theme
lxappearance
```
Seleccionamos el tema y el cursor



5. Ahora instalamos los iconos: [papirus](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
```
wget -qO- https://git.io/papirus-icon-theme-install | sh
```
Y el [programa para cambiar el color](https://github.com/PapirusDevelopmentTeam/papirus-folders)
```
wget -qO- https://git.io/papirus-folders-install | sh
```
6. Volvemos a abir lxappearance y seleccionamos los iconos
```
lxappearance
papirus-folders -C color 
```




7. Instalamos [vimplug](https://github.com/junegunn/vim-plug)
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
Dentro de vim --> :PlugInstall
```



8. Para configurar pywal, primero hacemos, con una imagen cualquiera
```
wal -i path_to_image
```
Ahora ya con alt+w podemos cambiar entre todos los wallpapers
Si no funciona, quizá es que tenemos que hacer ejecutables los archivos dentro de ./scripts

9. Instalamos [zathurapywal](https://github.com/GideonWolfe/Zathura-Pywal)
```
cd .scripts/zathurapywal 
./install.sh
```




# Algunos mandatos útiles
```
xdg-settings set default-web-browser org.qutebrowser.qutebrowser.desktop
xdg-user-dirs-update
xdg-mime default org.pwmt.zathura.desktop application/pdf
```








cambiar /home/jota por $HOME cuando sea posible
theme midnight gray a tar
remove kittyprev
remove .xinitrc
incluir fuente en git
