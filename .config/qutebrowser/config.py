# _  _________   ______ ___ _   _ ____ ___ _   _  ____ ____  
#| |/ / ____\ \ / / __ )_ _| \ | |  _ \_ _| \ | |/ ___/ ___| 
#| ' /|  _|  \ V /|  _ \| ||  \| | | | | ||  \| | |  _\___ \ 
#| . \| |___  | | | |_) | || |\  | |_| | || |\  | |_| |___) |
#|_|\_\_____| |_| |____/___|_| \_|____/___|_| \_|\____|____/ 
#Switch between tabs
config.bind('K', 'tab-next')
config.bind('J', 'tab-prev')

#Move tabs
config.bind('gk', 'tab-move +')
config.bind('gj', 'tab-move -')

#Show/hide tabs
config.bind('xx', 'config-cycle tabs.show always never')
#config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')

#qute-pass
config.bind('zl', 'spawn --userscript qute-pass --username-target secret --username-pattern "username: (.+)"') #This reads the username from the .gpg. The format inside the .gpg is username: actual_username
config.bind('zul', 'spawn --userscript qute-pass --username-only --username-target secret --username-pattern "username: (.+)"') 
config.bind('zpl', 'spawn --userscript qute-pass --password-only')#These are neccesary for sites like google where the username and the password are not introduced at the same time

#MPV
config.bind('xw', 'spawn mpv --ytdl-format=best {url}') #Watch a video
config.bind('xf', 'hint links spawn mpv --ytdl-format=best {hint-url}') #Watch a video, but select url with hints
config.bind('xd', 'spawn kitty -e youtube-dl {url}') #Download a video
config.bind('xmm', 'spawn kitty -e mpv --no-video {url}') #Listen to a video
config.bind('xmf', 'hint links spawn kitty -e mpv --no-video {hint-url}') #Listen to a video, but select url with hints
config.bind('xmd', 'spawn kitty -e youtube-dl -x --audio-format mp3 {url}') #Download a song
config.bind('xmp', 'spawn kitty -e mpv --no-video --shuffle {url}') #Listen to a playlist in random order

#Yank url with hints
config.bind('yf', 'hint links yank')

config.bind('cs', 'config-source')
#config.bind('<t>', 'set-cmd-text -s :open -t')


# ____  _____    _    ____   ____ _   _ 
#/ ___|| ____|  / \  |  _ \ / ___| | | |
#\___ \|  _|   / _ \ | |_) | |   | |_| |
# ___) | |___ / ___ \|  _ <| |___|  _  |
#|____/|_____/_/   \_\_| \_\\____|_| |_|
# _____ _   _  ____ ___ _   _ _____ ____  
#| ____| \ | |/ ___|_ _| \ | | ____/ ___| 
#|  _| |  \| | |  _ | ||  \| |  _| \___ \ 
#| |___| |\  | |_| || || |\  | |___ ___) |
#|_____|_| \_|\____|___|_| \_|_____|____/ 
c.url.searchengines = {
'DEFAULT': 'https://duckduckgo.com/?q={}',
'am': 'https://www.amazon.es/s?k={}',
'aw': 'https://wiki.archlinux.org/?search={}',
'goog': 'https://www.google.com/search?q={}',
're': 'https://www.reddit.com/r/{}',
'ub': 'https://www.urbandictionary.com/define.php?term={}',
'wiki': 'https://en.wikipedia.org/wiki/{}',
'yt': 'https://www.youtube.com/results?search_query={}',
'wr': 'https://www.wordreference.com/es/translation.asp?tranword={}',
'rae': 'https://dle.rae.es/{}',
'gen': 'http://libgen.rs/search.php?req={}',
'flv':'https://www3.animeflv.net/browse?q={}',
'cr': 'https://www.crunchyroll.com/search?from=&q={}',
'cal': 'https://www.casadellibro.com/?q={}'
}

#  ____ ___  _     ___  ____  ____  
# / ___/ _ \| |   / _ \|  _ \/ ___| 
#| |  | | | | |  | | | | |_) \___ \ 
#| |__| |_| | |__| |_| |  _ < ___) |
# \____\___/|_____\___/|_| \_\____/ 

colors = []
cache='/home/jota/.cache/wal/colors'
def load_colors(cache):
    with open(cache, 'r') as file:
        for i in range(16):
            colors.append(file.readline().strip())
load_colors(cache)

# Text color of the completion widget. May be a single color to use for
# all columns or a list of three colors, one for each column.
# Type: List of QtColor, or QtColor
c.colors.completion.fg = ['white', colors[4], colors[2]]

# Background color of the completion widget for odd rows.
# Type: QssColor
c.colors.completion.odd.bg = '#222222'

# Background color of the completion widget for even rows.
# Type: QssColor
c.colors.completion.even.bg = '#282828'

# Foreground color of completion widget category headers.
# Type: QtColor
c.colors.completion.category.fg = colors[3]

# Background color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.bg = 'qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #151515, stop:1 #202020)'

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.top = '#222222'

# Bottom border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.bottom = '#222222'

# Foreground color of the selected completion item.
# Type: QtColor
c.colors.completion.item.selected.fg = '#282c34'

# Background color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.bg = '#EEF085'

# Foreground color of the matched text in the selected completion item.
# Type: QtColor
c.colors.completion.item.selected.match.fg = '#FF2436'

# Foreground color of the matched text in the completion.
# Type: QtColor
c.colors.completion.match.fg = '#FF2436'

# Color of the scrollbar handle in the completion view.
# Type: QssColor
c.colors.completion.scrollbar.fg = 'white'

# Background color for the download bar.
# Type: QssColor
c.colors.downloads.bar.bg = '#222222'

# Background color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.bg = '#ff6c6b'

# Font color for hints.
# Type: QssColor
c.colors.hints.fg = 'white'

c.colors.hints.bg = colors[0]

c.hints.border = "1px solid #888888"

# Font color for the matched part of hints.
# Type: QtColor
c.colors.hints.match.fg = '#FF2436'

# Background color of an info message.
# Type: QssColor
c.colors.messages.info.bg = '#222222'

# Background color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.bg = '#222222'

# Foreground color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.fg = 'white'

# Background color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.bg = '#497920'

# Background color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.bg = '#34426f'

# Background color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.bg = '#222222'

# Foreground color of the URL in the statusbar when there's a warning.
# Type: QssColor
c.colors.statusbar.url.warn.fg = 'yellow'
  
c.colors.statusbar.url.error.fg = '#E52929'

c.colors.statusbar.url.fg = colors[6]

c.colors.statusbar.url.hover.fg = 'green'

c.colors.statusbar.url.success.http.fg = 'green'

c.colors.statusbar.url.success.https.fg = colors[2]

# Background color of the tab bar.
# Type: QssColor
c.colors.tabs.bar.bg = colors[0]

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = '#777777'

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = '#777777'

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = colors[0]

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = colors[0]

# config.set("colors.webpage.darkmode.enabled", True)

#  ___ _____ _   _ _____ ____  ____  
# / _ \_   _| | | | ____|  _ \/ ___| 
#| | | || | | |_| |  _| | |_) \___ \ 
#| |_| || | |  _  | |___|  _ < ___) |
# \___/ |_| |_| |_|_____|_| \_\____/ 
config.load_autoconfig(False)

#Statusbar widgets
c.statusbar.widgets = ["keypress", "url", "scroll", "history", "tabs", "progress"]

#Homepage
#c.url.default_page = 'file:///home/jota/Downloads/homepage/html/homepage.html'
